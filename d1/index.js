// Section Node.js

/*
	we use require directive to load Node.js modules

	"http" module lets Node.js transfer data using hypertext transfer protocol
	"http" module is a set of individual file that contain code to create a "component" that helps establish data transfer between applications

	HTTP is a protocol that allows the fetching of resources such as HTML documents

	Clients (browser) and server (Node.js/expressJS applications) communicate by exchanging individual messages

	message that came from the clients - requests
	messages that came from the server - response

*/

let http = require("http");

/*
	http - we are now tring to use the http module for us to create our server-side application

	createServer() - found inside http module; a method that accepts a function as its argument for a server
	(request, response) - arguments that are passed to the createServer method; this would allow us to receive requests (1st parameter) and send responses (2nd parameter)
*/

http.createServer(function(request, response){
	// we use writeHead() to: set a status code for the response - a 200 means OK status
	// set Content-type; using "text/plain" means that we are sending plain text as a response
	response.writeHead(200, {"Content-Type":"text/plain"});
	// we use response.end to the last stage on which sending the response from the server
	response.end("Hello world!");

// .listen() allows our application to run in our local devices through specified port.
// port - virtual point where network connections start and end. Each port is associated with a specific process/ service
}).listen(4000);

/*
	ctrl+c in gitbash to stop a process
*/

// used to confirm if the server is running on a port
console.log();

/*
	nodemon
	- installing nodemon will allow the server to automatically restart when files have been changed for update like saving the files

	syntax "npm install -g nodemon"

	"-g" means that we are going to install the package globally in our device

	"-nodemon" - is the package installed
*/